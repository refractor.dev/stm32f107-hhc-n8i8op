// std and main are not available for bare metal software
#![no_std]
#![no_main]

use cortex_m_rt::entry; // The runtime
use stm32f1xx_hal::{timer::Timer, pac, prelude::*}; // STM32F1 specific functions
use nb::block;
#[allow(unused_imports)]
use panic_halt; // When a panic occurs, stop the microcontroller

// This marks the entrypoint of our application. The cortex_m_rt creates some
// startup code before this, but we don't need to worry about this
#[entry]
fn main() -> ! {
    // Get handles to the hardware objects. These functions can only be called
    // once, so that the borrowchecker can ensure you don't reconfigure
    // something by accident.
    let dp = pac::Peripherals::take().unwrap();
    let cp = cortex_m::Peripherals::take().unwrap();



    // GPIO pins on the STM32F1 must be driven by the APB2 peripheral clock.
    // This must be enabled first. The HAL provides some abstractions for
    // us: First get a handle to the RCC peripheral:
    let rcc = dp.RCC.constrain();
    // Now we have access to the RCC's registers. The GPIOC can be enabled in
    // RCC_APB2ENR (Prog. Ref. Manual 8.3.7), therefore we must pass this
    // register to the `split` function.
    let mut gpioc = dp.GPIOC.split();
    let mut gpioe = dp.GPIOE.split();
    // This gives us an exclusive handle to the GPIOC peripheral. To get the
    // handle to a single pin, we need to configure the pin first. Pin C13
    // is usually connected to the Bluepills onboard LED.

    let mut relays = [
        gpioe.pe12.into_push_pull_output(&mut gpioe.crh).erase(),
        gpioe.pe13.into_push_pull_output(&mut gpioe.crh).erase(),
        gpioe.pe14.into_push_pull_output(&mut gpioe.crh).erase(),
        gpioe.pe15.into_push_pull_output(&mut gpioe.crh).erase(),
        gpioc.pc6.into_push_pull_output(&mut gpioc.crl).erase(),
        gpioc.pc7.into_push_pull_output(&mut gpioc.crl).erase(),
        gpioc.pc8.into_push_pull_output(&mut gpioc.crh).erase(),
        gpioc.pc9.into_push_pull_output(&mut gpioc.crh).erase(),
    ];
    // Now we need a delay object. The delay is of course depending on the clock
    // frequency of the microcontroller, so we need to fix the frequency
    // first. The system frequency is set via the FLASH_ACR register, so we
    // need to get a handle to the FLASH peripheral first:
    let mut flash = dp.FLASH.constrain();

    // Now we can set the controllers frequency to 8 MHz:
    let clocks = rcc.cfgr.use_hse(16.MHz()).freeze(&mut flash.acr);

    let mut timer = Timer::syst(cp.SYST, &clocks).counter_hz();
    timer.start(1.Hz()).unwrap();

    // Now, enjoy the lightshow!
    loop {
        for  relay in relays.iter_mut() {
            relay.set_high();
        }
        block!(timer.wait()).unwrap();
        for  relay in relays.iter_mut() {
            relay.set_low();
        }
        block!(timer.wait()).unwrap();
    }
}
